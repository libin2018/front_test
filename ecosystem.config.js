module.exports = {
  apps : [],
  deploy : {
    test: {
      user: 'ubuntu',
      host: '111.230.145.53',
      ref: 'origin/master',
      repo: 'git@gitlab.com:libin2018/front_test.git',
      path: '/home/ubuntu/projects/front_test',
      'post-deploy': 'git log -2',
    }
  }
};